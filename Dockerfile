#v4
FROM debian
LABEL Autor=benjamin@cybercorp.fr

RUN apt-get update && apt-get -y install cron python3-pip curl jq
RUN apt-get -y install libffi-dev rsync python3-requests python3-icalendar
RUN pip3 install ccxt --break-system-packages
RUN apt-get install -y mailutils msmtp msmtp-mta sendemail gdebi-core wget curl ssh
RUN mkdir /etc/msmtpdir ; touch /etc/msmtpdir/msmtprc ; ln -s /etc/msmtpdir/msmtprc /etc/msmtprc
RUN touch /var/log/cron.log

RUN curl -s https://packagecloud.io/install/repositories/ookla/speedtest-cli/script.deb.sh | bash
RUN apt-get install speedtest

CMD ["cron", "-f"]
